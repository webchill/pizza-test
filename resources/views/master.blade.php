<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('head_title')</title>

    <link href="{{ URL::to('css/app.css') }}" rel="stylesheet" type="text/css">

    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>
<body>
<div id="app">
    <div class="container">
        @include('partials.head')
        <div class="row">
            <div class="col-sm-12 text-center">
                @if (!Request::is('/'))
                    <a class="pull-left" href="{{ route('layouts.welcome') }}">Home</a>
                @endif
                <h1>@yield('title_on_page')</h1>
            </div>
        </div>
        @yield('content')
    </div>
</div>
@yield('scripts')
<script src="{{ URL::to('js/app.js') }}"></script>
</body>
</html>