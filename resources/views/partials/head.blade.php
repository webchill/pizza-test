<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ route('layouts.welcome') }}">Pizza yum!</a>
        </div>
        <div id="navbar" class="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li {{ (Request::is('/') ? 'class=active' : '') }}><a href="{{ route('layouts.welcome') }}">Home</a></li>
                <li {{ (Request::is('build*') ? 'class=active' : '') }}><a href="{{ route('layouts.build') }}">Build the pizza</a></li>
                <li>
                    <a href="#">
                        Cart:
                        {{ Session::has('cart') ? '<strong>'.Session::get('cart')->totalPrice.' &euro;</strong>' : '' }}
                        <span class="badge">
                            {{ Session::has('cart') ? Session::get('cart')->totalQty : 0 }}
                        </span>
                    </a>
                </li>
                {{ Session::has('cart') ? ' <li><a href="#">Empty the cart</a></li>' : '' }}
            </ul>
        </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
</nav>