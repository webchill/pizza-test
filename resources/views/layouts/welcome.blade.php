@extends('master')

@section('head_title')
    Select pizza
@endsection

@section('title_on_page')
    Select pizza you want
@endsection

@section('content')
    <div class="row">
        @foreach($pizzas as $pizza)
            <?php $ingrs = null; $price = 0; ?>
            @foreach($ingredients as $key => $ingredient)
                @if ($ingredient->pizza_id == $pizza->id )
                    <?php $ingrs .= $ingrs ? ', ' . $ingredient->name : $ingredient->name ; ?>
                    <?php $price += $ingredient->price ?>
                @endif
            @endforeach
            <onepizza
                    image="{{$pizza->imagePath}}"
                    link="{{ route('layouts.build', ['id' => $pizza->id]) }}"
                    ingredients="{{$ingrs}}"
                    price="{{($price + ($price / 2)) * 0.01 }}">{{$pizza->name}}</onepizza>
        @endforeach
    </div>
@endsection
