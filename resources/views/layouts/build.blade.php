@extends('master')

@section('head_title')
    Building the pizza
@endsection

@section('title_on_page')
    Build your pizza!
@endsection

@section('content')

    <buildpizza
            price="{{$price}}"
            pizza="{{$pizza ? json_encode($pizza) : $pizza}}"
            checked="{{$checked ? json_encode($checked) : $checked}}"
            image="{{$image ? $image : 'https://lulu.lv/uploads/storage/product_file/asset/201109/24/Plana.png'}}"
            ingredients="{{ $ingredients ? json_encode($ingredients) : $ingredients }}"></buildpizza>
@endsection

<?php /*

    <div class="row build-container">
        <div class="col-sm-4 col-sm-offset-8 text-right">
            <p>
                Price: {{ $price }} &euro;
            </p>
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="pizza_1_count" class="col-sm-7 control-label">How much?</label>
                    <div class="col-sm-5">
                        <select id="pizza_1_count" class="form-control">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                </div>
            </div>
            <button class="btn btn-success">Add to cart</button>
        </div>
        <div class="col-md-7">
            <div class="checkbox">
                <div class="row">
                    <div class="col-md-6">
                        <ul>
                            @foreach($ingredients as $key => $ingredient)
                                    <li>
                                        <label>
                                            <input type="checkbox" <?= $pizza && !empty($pizza[$ingredient->id]) ? 'checked' : '' ?> >
                                            {{ $ingredient->name }} ({{$ingredient->price * 0.01}} &euro;)
                                        </label>
                                    </li>
                                @if (($key+1) == ($ingredient_length / 2))
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5 text-right">
            <img src="{{ $image ? $image : 'https://lulu.lv/uploads/storage/product_file/asset/201109/24/Plana.png' }}" alt="pizza">
        </div>
    </div>
 */ ?>
