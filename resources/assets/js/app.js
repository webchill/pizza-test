import Vue from 'vue';

Vue.component('onepizza', require('./components/OnePizza.vue'));
Vue.component('buildpizza', require('./components/BuildPizza.vue'));

const app = new Vue({
    el: '#app',
    data: {
        pizzas: {}
    },
    methods: {
        onClick: function (str) {
            alert(str);
        }
    }
});
