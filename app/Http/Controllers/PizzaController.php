<?php

namespace App\Http\Controllers;

use App\Pizza;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PizzaController extends Controller
{
    public function getPizzas() {
        $pizzas = DB::table('pizzas')
            ->get();

        $ingredients = DB::table('pizzas_ingredients_relation')
            ->join('ingredients', 'pizzas_ingredients_relation.ingredient_id', '=', 'ingredients.id')
            ->get();

        return view('layouts.welcome', ['pizzas' => $pizzas, 'ingredients' => $ingredients]);
    }
    
    public function getBuild(Request $request, $id = null) {
        if ($id) {
            $pizzas = DB::table('pizzas')
                ->join('pizzas_ingredients_relation', 'pizzas_ingredients_relation.pizza_id', '=', 'pizzas.id')
                ->join('ingredients', 'pizzas_ingredients_relation.ingredient_id', '=', 'ingredients.id')
                ->where('pizzas.id', '=', $id)
                ->get();

            $price = 0;
            $checked = [];
            foreach ($pizzas as $k => $v) {
                $pizza[$v->ingredient_id] = $v;
                $checked[] = $v->ingredient_id;
                $price += $v->price;
            }
            $price = ($price + $price / 2)  * 0.01;

        }

        $ingredients = DB::table('ingredients')
            ->get();

        return view('layouts.build', [
            'ingredients' => $ingredients,
            'pizza' => isset($pizza) ? $pizza : null,
            'price' => isset($price) ? $price : 0,
            'image' => isset($pizzas) ? $pizzas[0]->imagePath : null,
            'checked' => isset($checked) ? $checked : null
            ]);
    }
}
