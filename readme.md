## Pizza constructor app
With laravel and vue.js
## Install
* git pull
* composer -v update
* configure .env file for database (mysql)
* php artisan migrate
* php artisan db:seed --class=PizzaTableSeeder
* php artisan db:seed --class=IngredientTableSeeder
* php artisan db:seed --class=PizzaIngredientRelationTableSeeder
