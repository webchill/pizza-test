<?php

use Illuminate\Database\Seeder;

class PizzaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pizzas')->insert([
            'imagePath' => 'http://lulu.lv/uploads/storage/product_file/asset/201110/73/thumb_senu__Copy_.png',
            'name' => 'MacDac Pizza',
        ]);
        DB::table('pizzas')->insert([
            'imagePath' => 'https://lulu.lv/uploads/storage/product_file/asset/201110/88/thumb_trio2__Copy_.png',
            'name' => 'Lovely Mushroom Pizza',
        ]);
    }
}
