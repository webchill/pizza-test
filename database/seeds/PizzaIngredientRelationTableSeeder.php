<?php

use Illuminate\Database\Seeder;

class PizzaIngredientRelationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // macdac pizza
        DB::table('pizzas_ingredients_relation')->insert([
            'pizza_id' => 1,
            'ingredient_id' => 1,
        ]);
        DB::table('pizzas_ingredients_relation')->insert([
            'pizza_id' => 1,
            'ingredient_id' => 2,
        ]);
        DB::table('pizzas_ingredients_relation')->insert([
            'pizza_id' => 1,
            'ingredient_id' => 3,
        ]);
        DB::table('pizzas_ingredients_relation')->insert([
            'pizza_id' => 1,
            'ingredient_id' => 4,
        ]);
        DB::table('pizzas_ingredients_relation')->insert([
            'pizza_id' => 1,
            'ingredient_id' => 5,
        ]);
        DB::table('pizzas_ingredients_relation')->insert([
            'pizza_id' => 1,
            'ingredient_id' => 6,
        ]);
        DB::table('pizzas_ingredients_relation')->insert([
            'pizza_id' => 1,
            'ingredient_id' => 7,
        ]);

        // mushroom pizza

        DB::table('pizzas_ingredients_relation')->insert([
            'pizza_id' => 2,
            'ingredient_id' => 1,
        ]);
        DB::table('pizzas_ingredients_relation')->insert([
            'pizza_id' => 2,
            'ingredient_id' => 8,
        ]);
        DB::table('pizzas_ingredients_relation')->insert([
            'pizza_id' => 2,
            'ingredient_id' => 7,
        ]);
        DB::table('pizzas_ingredients_relation')->insert([
            'pizza_id' => 2,
            'ingredient_id' => 2,
        ]);
        DB::table('pizzas_ingredients_relation')->insert([
            'pizza_id' => 2,
            'ingredient_id' => 6,
        ]);
    }
}
