<?php

use Illuminate\Database\Seeder;

class IngredientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ingredients')->insert([
            'name' => 'Tomato',
            'price' => 50,
        ]);
        DB::table('ingredients')->insert([
            'name' => 'Sliced mushrooms',
            'price' => 50,
        ]);
        DB::table('ingredients')->insert([
            'name' => 'Feta cheese',
            'price' => 150,
        ]);
        DB::table('ingredients')->insert([
            'name' => 'Sausages',
            'price' => 100,
        ]);
        DB::table('ingredients')->insert([
            'name' => 'Sliced onion',
            'price' => 50,
        ]);
        DB::table('ingredients')->insert([
            'name' => 'Oregano',
            'price' => 200,
        ]);
        DB::table('ingredients')->insert([
            'name' => 'Mozzarella cheese',
            'price' => 30,
        ]);
        DB::table('ingredients')->insert([
            'name' => 'Bacon',
            'price' => 100,
        ]);
    }
}
