<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses' => 'PizzaController@getPizzas',
    'as' => 'layouts.welcome'
]);

Route::get('/build/{id?}', [
    'uses' => 'PizzaController@getBuild',
    'as' => 'layouts.build'
]);
